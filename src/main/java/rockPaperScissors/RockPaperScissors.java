package rockPaperScissors;

import java.io.PipedWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.printf("Let's play round %d", roundCounter);
            
            // get human choice
            String humanChoice = getHumanChoice();

            // get computer choice
            String computerChoice = getComputerChoice();

            // get winner
            if (getWinner(humanChoice, computerChoice)) {
                System.out.printf("Human chose %s, computer chose %s. Human wins!",humanChoice, computerChoice);
                humanScore++;
            }
            else if (getWinner(computerChoice, humanChoice)) {
                System.out.printf("Human chose %s, computer chose %s. Computer wins!",humanChoice, computerChoice);
                computerScore++;
            }
            else {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!",humanChoice, computerChoice);
            }

            // print score
            System.out.printf("\nScore: human %d, computer %d", humanScore, computerScore);

            // ask to play again
            String answer = continuePlaying();
            if (answer.equals("n")) {
                break;
            }
            roundCounter++;
        }
        System.out.println("Bye bye :)");
    }

    public String getHumanChoice() {
        while (true) {
            String humanChoice = readInput("\nYour choice (Rock/Paper/Scissors)?");
            if (validInput(humanChoice, rpsChoices)) {
                return humanChoice.toLowerCase();
            }
            System.out.printf("I don't understand %s. Try again", humanChoice);
        }
    }

    public boolean validInput(String input, List<String> validChoices) {
        for (String validChoice : validChoices) {
            if (input.toLowerCase().equals(validChoice)) {
                return true;
            }
        }
        return false;
    }

    public String getComputerChoice() {
        String[] choices = {"rock", "paper", "scissors"};
        Double randNum = Math.random()*3;
        int randInt = randNum.intValue();       
        // int randomInt = new Random().nextInt(choices.length);
        return choices[randInt];
    }

    public boolean getWinner(String winningChoice, String losingChoice) {
        if ((winningChoice.equals("rock") && losingChoice.equals("scissors")) ||
        (winningChoice.equals("paper") && losingChoice.equals("rock")) ||
        (winningChoice.equals("scissors") && losingChoice.equals("paper"))) {
            return true;
        }
        return false;
    }

    public String continuePlaying() {
        while (true) {
            String continuePlaying = readInput("\nDo you wish to continue playing? (y/n)?");
            List<String> contChoices = Arrays.asList("y", "n");
            if (validInput(continuePlaying, contChoices)) {
                return continuePlaying;
            }
            System.out.printf("I don't understand %s. Try again", continuePlaying);
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
